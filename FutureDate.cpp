#include "FutureDate.h"
#include <ctime>
#include <iostream>

FutureDate::FutureDate() : Date()
{
}
FutureDate::FutureDate(int month, int day, int year) : Date(month,day,year) 
{
	if (FutureDate::timeCompare()==1)
	{
		cout<<"Enter a date in the future. Try again. \n";
		for (int x=-1; x!=0;)
		{
			if (x==2) 
				cout<<"Invalid date range. Try again.\n";
			if (x==1)
				cout<<"Please set a future date.\n";
			cout<<"Enter a month: ";
			cin>>month;
			cout<<"Enter a day: ";
			cin>>day;
			cout<<"Enter a year: ";
			cin>>year;
			if (month==1||month==3||month==5||month==7||month==8||month==10||month==12)
			{
				if (day>1 && day<=31)
				{
					FutureDate::setDate(month,day,year);
					x=FutureDate::timeCompare();
				}
			}
			else if (month==9||month==6||month==4||month==11)
			{
				if (day>1 && day<=30)
				{
					FutureDate::setDate(month,day,year);
					x=FutureDate::timeCompare();
				}
			}
			else if (month==2)
			{
				if (day>1 && day<=28)
				{
					FutureDate::setDate(month,day,year);
					x=FutureDate::timeCompare();
				}	
			}
			else
			{
				x=2;
				continue;
			} 
		}
	}
}
int FutureDate::timeCompare()
{
	time_t timeCode = time(0);
	struct tm *now = localtime(&timeCode);
	now->tm_year += 1900;
	now->tm_mon += 1;
	//cout<<now->tm_year<<"-"<<now->tm_mon<<"-"<<now->tm_mday<<endl
	//	<<FutureDate::year<<"-"<<FutureDate::month<<"-"<<FutureDate::day<<endl;
	if (now->tm_year > FutureDate::year)
	{
		return 1;
	}
	if (now->tm_year == FutureDate::year)
	{
		if (now->tm_mon > FutureDate::month)
		{
			return 1;
		}
		if (now->tm_mon == FutureDate::month)
		{
			if (now->tm_mday > FutureDate::day || now->tm_mday == FutureDate::day)
			{
				return 1;
			}
			if (now->tm_mday < FutureDate::day)
			{
				return 0;
			}
		}
		if (now->tm_mon < FutureDate::month)
		{
			return 0;
		}
	}
	if (now->tm_year < FutureDate::year)
	{
		return 0;
	}

}
FutureDate::~FutureDate()
{
	//nothing
}