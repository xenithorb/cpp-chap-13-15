#include "date.h"
#include <ctime>
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
// Mutators 
void Date::setDate(int mon, int d, int yr)
{
	Date::day = d;
	Date::month = mon;
	Date::year = yr;
}
// Constructors 
Date::Date() //default constructor
{
  Date::setDate(1,1,2000);
}
Date::Date(int month, int day, int year) // overloaded constructor
{
	int count=0;
	do 
	{
		if (count!=0)
		{
			std::cout<<"Enter a month: ";
			cin>>month;
			std::cout<<"Enter a day: ";
			cin>>day;
			std::cout<<"Enter a year: ";
			cin>>year;
		}
		if (month==1||month==3||month==5||month==7||month==8||month==10||month==12)
		{
			if (day>1 && day<=31)
			{
				Date::setDate(month,day,year);
				count=0;
			}
		}
		else if (month==9||month==6||month==4||month==11)
		{
			if (day>1 && day<=30)
			{
				Date::setDate(month,day,year);
				count=0;
			}
		}
		else if (month==2)
		{
			if (day>1 && day<=28)
			{
				Date::setDate(month,day,year);
				count=0;
			}	
		}
		else
		{
			std::cout<<"Try Again, Invalid Date Range.\n";
			count=1;
		} 
	} while (count!=0);
}
// Destructor 
Date::~Date()
{

}
// Accessors 
int Date::getDay()
{
	return Date::day;
}
int Date::getMonth()
{
	return Date::month;
}
int Date::getYear()
{
	return Date::year;
}
// Convert month to string
string Date::convertMonth(int month)
{
	char *monthName[] = {"January","February","March","April","May","June","July","August",
		"September","October","November","December"};
	return monthName[month-1];
}
// Display date in proper format on console.
void Date::displayDate()
{
	cout<<convertMonth(Date::month)<<" "<<Date::day<<", "<<Date::year;
}
// Overloaded operators
void Date::incrementP()
{
	if (Date::month==1||Date::month==3||Date::month==5||Date::month==7||Date::month==8||Date::month==10)
	{
		if (Date::day<31)
		{ 
			Date::day++;
		}
		else if (Date::day==31)
		{ 
			Date::day = 1;
			Date::month++;
		}
	}
	if (Date::month==2)
	{
		if (Date::day<28)
		{ 
			Date::day++;
		}
		else if (Date::day==28)
		{ 
			Date::day = 1;
			Date::month++;
		}
	}
	if (Date::month==4||Date::month==6||Date::month==9||Date::month==11)
	{
		if (Date::day<30)
		{ 
			Date::day++;
		}
		else if (Date::day==30)
		{ 
			Date::day = 1;
			Date::month++;
		}
	}
	if (Date::month==12)
	{
		if (Date::day<31)
		{ 
			Date::day++;
		}
		else if (Date::day==31)
		{ 
			Date::day = 1;
			Date::month = 1;
			Date::year++;
		}
	}
}
Date Date::operator++ ()
{
	Date::incrementP();
	return *this;
}
Date Date::operator++ (int)
{
	Date current = *this;
	Date::incrementP();
	return current;
}
void Date::decrementS()
{
	if (Date::month==2||Date::month==4||Date::month==6||Date::month==9||Date::month==11)
	{
		if (Date::day==1)
		{ 
			Date::day=31;
			Date::month--;
		}
		else if (Date::day>1)
		{ 
			Date::day--;
		}
	}
	if (Date::month==5||Date::month==7||Date::month==10||Date::month==12)
	{
		if (Date::day==1)
		{ 
			Date::day=30;
			Date::month--;
		}
		else if (Date::day>1)
		{ 
			Date::day--;
		}
	}
	if (Date::month==3)
	{
		if (Date::day==1)
		{ 
			Date::day=28;
			Date::month--;
		}
		else if (Date::day>1)
		{ 
			Date::day--;
		}
	}
	if (Date::month==1)
	{
		if (Date::day==1)
		{
			Date::day=31;
			Date::month=12;
			Date::year--;
		}
		else if (Date::day>1)
		{
			Date::day--;
		}
	}
}
Date Date::operator-- ()
{
	Date::decrementS();
	return *this;
}
Date Date::operator-- (int)
{
	Date current = *this;
	Date::decrementS();
	return current;
}
ostream& operator<<(ostream& out, const Date& value)
{
	out << value.convertMonth(value.month) << " " << value.day << ", " << value.year;
	return out;
}
istream& operator>>(istream& in, Date& value)
{
	for (int x=1; x==1; x++)
	{
		std::cout<<"Enter the Month: ";
		in >> value.month;
		if (value.month>12)
		{
			std::cout<<"Invalid Month. Try Again.\n";
			x--;
		}
	}
	for (int x=1; x==1; x++)
	{
		std::cout<<"Enter the Day: ";
		in >> value.day;
		if (value.month==1||value.month==3||value.month==5||value.month==7||value.month==8||value.month==10||value.month==12)
		{
			if (value.day>31 || value.day<1)
			{
			std::cout<<"Invalid Day. Try Again.\n";
			x--;
			}
		}
		else if (value.month==4||value.month==6||value.month==9||value.month==11)
		{
			if (value.day>30 || value.day<1)
			{
			std::cout<<"Invalid Day. Try Again.\n";
			x--;
			}
		}
		else if (value.month=2)
		{
			if (value.day>28 || value.day<1)
			{
			std::cout<<"Invalid Day. Try Again.\n";
			x--;
			}
		}
	}
	for (int x=1; x==1; x++)
	{
		std::cout<<"Enter the Year: ";
		in >> value.year;
		if (value.year<1||value.year>=3000)
		{
			std::cout<<"Invalid Year. Try Again.\n";
			x--;
		}
	}
	return in;
}
