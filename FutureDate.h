#include "date.h"
#ifndef FUTUREDATE_H
#define FUTUREDATE_H


class FutureDate : public Date
{
public:
	FutureDate();
	FutureDate(int,int,int);
	~FutureDate();
	int timeCompare();
};

#endif