#include <iostream>
#include <string>
#ifndef DATE_H
#define DATE_H

using namespace std;
class Date 
{
public:
	int month, day, year;
	// Accessors and Mutators
	void setDate(int,int,int);
	void displayDate(); // cout's the set date 
	static string convertMonth(int);
	int getDay();
	int getMonth();
	int getYear();
	// Default and overloaded Constructors
	Date();
	Date(int,int,int);
	// Destructor 
	~Date();
	// Overloaded Operators
	Date operator++ ();
	Date operator++ (int);
	void incrementP();
	Date operator-- ();
	Date operator-- (int);
	void decrementS();
	friend ostream& operator<<(ostream&, const Date&);
	friend istream& operator>>(istream&, Date&);
};

#endif.